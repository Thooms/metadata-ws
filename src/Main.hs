module Main where

import Network.Wai.Handler.Warp
import System.Console.GetOpt
import System.Environment

import API (app)

-- Argument parsing boilerplate

data PortArg = PortArg Int deriving (Show)

options :: [OptDescr PortArg]
options = [
  Option ['p'] ["port"] (ReqArg (PortArg . read) "PORTNUM") "Port number on which to listen"
  ]

compilerOpts :: [String] -> IO ([PortArg], [String])
compilerOpts argv = case getOpt Permute options argv of
  (o,n,[]  ) -> return (o,n)
  (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
    where header = "Usage: metadata-ws [OPTION...]"


main :: IO ()
main = do
  (opts, _) <- compilerOpts =<< getArgs
  case opts of
    (PortArg n):_ -> run n app
    _ -> putStrLn "Please provide a port number"
