{-# LANGUAGE DeriveGeneric #-}

module Types where

import Data.Aeson (FromJSON, ToJSON, encode)
import GHC.Generics

data MetaAttr = MetaAttr { key :: String, value :: String } deriving (Eq, Show, Generic)

data MetaTag = MetaTag [MetaAttr] deriving (Eq, Show, Generic)

data Metadata = Metadata { metadata :: [MetaTag] } deriving (Eq, Show, Generic)

instance ToJSON MetaAttr
instance ToJSON MetaTag
instance ToJSON Metadata

-- JSON error generation

data JSONError = JSONError { err :: String } deriving (Eq, Show, Generic)

instance ToJSON JSONError

errorStr s = encode $ JSONError { err = s }
