{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

-- This module exposes the WAI app so that the main just runs it

module API (app) where

import Control.Exception (catch, SomeException)
import Control.Lens
import Control.Monad.IO.Class
import Control.Monad.Trans.Either
import Network.Wai hiding (responseStatus)
import Network.Wreq (get, responseStatus, responseBody, statusCode)
import Network.URI (isURI)
import Servant
import Text.HTML.TagSoup
import qualified Data.ByteString.Lazy.Char8 as C

import Types

-- Only retain <meta> tags

filterTags = filter (~== (TagOpen ("meta" :: String) []))

-- Turn <meta> tags into Metadata entry

processTags :: [C.ByteString] -> [Tag C.ByteString]  -> Metadata
processTags filters l = Metadata { metadata = tags }
  where tags = l >>= processTag
        processTag t = case t of
          TagOpen _ l -> case map processKV (filter isWanted l) of
            [] -> []
            processedKV -> return $ MetaTag processedKV
          _ -> []
        isWanted (k, _) = null filters || k `elem` filters
        processKV (k, v) = MetaAttr { key = C.unpack k, value = C.unpack v }

-- Fetch and process the URL

fetchMetadata :: String -> [C.ByteString] -> EitherT ServantErr IO Metadata
fetchMetadata url filters = do

  -- Since `get` can throw an exception, we get back to purity with this fancy onliner
  req_ <- liftIO $ catch (return . Just =<< get url) (\(exc :: SomeException) -> return Nothing)

  case req_ of
    Nothing -> left $ err400 { errBody = errorStr "Unreachable URL" }
    Just req -> case req ^. responseStatus . statusCode of
      200 -> return $ processTags filters . filterTags . parseTags $ req ^. responseBody
      _ -> left $ err400 { errBody = errorStr "Invalid URL" }

-- The handler that will be called for our endpoint

handler :: Maybe String -> [String] -> EitherT ServantErr IO Metadata
handler Nothing _ = left $ err400 { errBody = errorStr "Missing URL"}
handler (Just url) filters
  | isURI url = fetchMetadata url (map C.pack filters)
  | otherwise = left $ err400 { errBody = errorStr "Invalid URL"}

-- The main API type

type MetadataAPI = "metadata"
                   :> QueryParam "url" String
                   :> QueryParams "filter" String
                   :> Get '[JSON] Metadata

-- Boilerplate app definition (links the API type to the handler)

server :: Server MetadataAPI
server = handler

metadataAPI :: Proxy MetadataAPI
metadataAPI = Proxy

app :: Application
app = serve metadataAPI server
