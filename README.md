# Simple metadata fetcher webservice

## Build

```
$ stack build
```

## Run

```
$ metadata-ws -p PORTNUM
```
